// user-detail.component.ts
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserListServiceService } from '../../shared/user-list-service.service';
import { User } from '../user.model';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css'],
})
// user-detail.component.ts
export class UserDetailComponent implements OnInit {
  userId: number | undefined;
  user: User | undefined;

  constructor(
    private route: ActivatedRoute,
    private userService: UserListServiceService
  ) {}

  ngOnInit(): void {
    // Utilisez ActivatedRoute pour récupérer l'ID de l'URL
    this.route.paramMap.subscribe((params) => {
      this.userId = Number(params.get('id'));

      // Appel du service pour récupérer les détails de l'utilisateur
      if (this.userId) {
        this.userService.getUserById(this.userId).subscribe((user) => {
          this.user = user;
        });
      }
    });
  }
}
