import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserListServiceService } from '../../shared/user-list-service.service';
import { User } from '../user.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css'],
})
export class EditUserComponent implements OnInit {
  userId: number | undefined;
  userForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private userService: UserListServiceService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.userForm = this.formBuilder.group({
      name: ['', Validators.required],
      occupation: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      bio: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.userId = Number(params.get('id'));

      if (this.userId) {
        this.userService.getUserById(this.userId).subscribe((user) => {
          this.userForm.patchValue(user);
        });
      }
    });
  }

  onSubmit(): void {
    if (this.userForm.valid && this.userId) {
      const updatedUser: User = { ...this.userForm.value, id: this.userId };

      this.userService.updateUser(updatedUser).subscribe((updated) => {
        console.log('Utilisateur mis à jour :', updated);
        this.router.navigate(['/users']);
      });
    }
  }
}
