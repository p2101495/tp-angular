import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserListServiceService } from '../../shared/user-list-service.service';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css'],
})
export class UserAddComponent {
  userForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserListServiceService,
    private router: Router
  ) {
    this.userForm = this.formBuilder.group({
      name: ['', Validators.required],
      occupation: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      bio: [''],
    });
  }

  onSubmit(): void {
    if (this.userForm.valid) {
      const newUser = this.userForm.value;
      console.log('New user:', newUser);

      this.userService.addUser(newUser).subscribe((response) => {
        console.log('User added successfully:', response);
        this.router.navigate(['/']);
      });
    }
  }
}
