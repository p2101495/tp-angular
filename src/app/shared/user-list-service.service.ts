import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../user-list/user.model';

@Injectable({
  providedIn: 'root',
})
export class UserListServiceService {
  constructor(private http: HttpClient) {}

  baseUrl = 'https://658158163dfdd1b11c430309.mockapi.io/user';

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${this.baseUrl}`);
  }

  getUserById(id: number): Observable<User> {
    return this.http.get<User>(`${this.baseUrl}/${id}`);
  }

  updateUser(user: User): Observable<User> {
    const url = `${this.baseUrl}/${user.id}`;
    return this.http.put<User>(url, user);
  }

  deleteUser(userId: number): Observable<void> {
    const url = `${this.baseUrl}/${userId}`;
    return this.http.delete<void>(url);
  }

  addUser(user: User) {
    return this.http.post<User>(this.baseUrl, user);
  }
}
